How to get ACPI data running on a chromebook
============================================

Requires acpi tools : sudo apt install acpica-tools


mkdir -p /tmp/acpi-extract/
cd /tmp/acpi-extract/
sudo acpidump > acpi.log
acpixtract acpi.log
iasl -d *.dat

Then you have *.dsl files containing acpi values.
